package it.unibo.oop.lab.collections1;

import java.util.*;

/**
 * Example class using {@link List} and {@link Map}.
 * 
 */
public final class UseCollection {

	private final static int START_NUMBER = 1000;
	private final static int END_NUMBER = 2000;
	private final static int START_INDEX = 0;
	private final static int ELEMS = 100000;
	private static final int TO_MS = 1000000;
	
    private UseCollection() {
    }

    /**
     * @param s
     *            unused
     */
    public static void main(final String... s) {
        /*
         * 1) Create a new ArrayList<Integer>, and populate it with the numbers
         * from 1000 (included) to 2000 (excluded).
         */
    	
    	List<Integer> list = new ArrayList<>();
    	for (int count = START_NUMBER; count < END_NUMBER; count++) {
    		list.add(count);
    	}
    	
        /*
         * 2) Create a new LinkedList<Integer> and, in a single line of code
         * without using any looping construct (for, while), populate it with
         * the same contents of the list of point 1.
         */
    	
    	List<Integer> linkedList = new LinkedList<>(list);
    	
        /*
         * 3) Using "set" and "get" and "size" methods, swap the first and last
         * element of the first list. You can not use any "magic number".
         * (Suggestion: use a temporary variable)
         */
    	final int endIndex = list.size() - 1;
    	final int temp = list.get(START_INDEX);
    	list.set(START_INDEX, list.get(endIndex));
    	list.set(endIndex , temp);
    	
        /*
         * 4) Using a single for-each, print the contents of the arraylist.
         */
    	for (Integer i : list) {
    		System.out.println(i);
    	}
        /*
         * 5) Measure the performance of inserting new elements in the head of
         * the collection: measure the time required to add 100.000 elements as
         * first element of the collection for both ArrayList and LinkedList,
         * using the previous lists. In order to measure times, use as example
         * TestPerformance.java.
         */
    	
    	long time = System.nanoTime();
        for (int i = 1; i <= ELEMS; i++) {
            list.add(START_INDEX, i);
        }
        
        
        time = System.nanoTime() - time;
        System.out.println("Converting " + ELEMS
                + " int to String and inserting them in a Set took " + time
                + "ns (" + time / TO_MS + "ms)");
       
    	
        time = System.nanoTime();
        for (int i = 1; i <= ELEMS; i++) {
            linkedList.add(START_INDEX, i);
        }
        
        time = System.nanoTime() - time;
        System.out.println("Converting " + ELEMS
                + " int to String and inserting them in a Set took " + time
                + "ns (" + time / TO_MS + "ms)");
        
        /*
         * 6) Measure the performance of reading 1000 times an element whose
         * position is in the middle of the collection for both ArrayList and
         * LinkedList, using the collections of point 5. In order to measure
         * times, use as example TestPerformance.java.
         */
        time = System.nanoTime();
        for (int i = 1; i <= START_NUMBER; i++) {
            list.get(list.size() / 2);
        }
        
        
        time = System.nanoTime() - time;
        System.out.println("Converting " + ELEMS
                + " int to String and inserting them in a Set took " + time
                + "ns (" + time / TO_MS + "ms)");
       
    	
        time = System.nanoTime();
        for (int i = 1; i <= START_NUMBER; i++) {
            linkedList.get(list.size() / 2);
        }
        
        time = System.nanoTime() - time;
        System.out.println("Converting " + ELEMS
                + " int to String and inserting them in a Set took " + time
                + "ns (" + time / TO_MS + "ms)");
        
        
        /*
         * 7) Build a new Map that associates to each continent's name its
         * population:
         * 
         * Africa -> 1,110,635,000
         * 
         * Americas -> 972,005,000
         * 
         * Antarctica -> 0
         * 
         * Asia -> 4,298,723,000
         * 
         * Europe -> 742,452,000
         * 
         * Oceania -> 38,304,000
         */
        
        Map<String, Long> map = new HashMap<>();
        map.put("Africa", 1110635000L);
        map.put("Americas", 972005000L);
        map.put("Antarctica", 0L);
        map.put("Asia", 4298723000L);
        map.put("Europe", 742452000L);
        map.put("Oceania", 38304000L);
        
        /*
         * 8) Compute the population of the world
         */
        long worldPopulation = 0;
        for (Long l : map.values()) {
        	worldPopulation += l;
        }
        System.out.println(worldPopulation);
    }
}
